package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type Config struct {
	Roms_path string
	Client_id string
	Client_secret string
}

var config_file string = "./config.json"

func LoadConfig() Config {
	content, err := ioutil.ReadFile(config_file)
	if err != nil {
		log.Fatal("Error Opening config.json")
	}

	// var config Config
	var config Config
	err = json.Unmarshal(content, &config)
	if err != nil {
		log.Fatal("Error loading config.json", err)
	}

	return config
}