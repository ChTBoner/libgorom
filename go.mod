module gitlab.com/ChTBoner/libgorom

go 1.18

require (
	github.com/Henry-Sarabia/igdb/v2 v2.0.0-alpha.4
	golang.org/x/oauth2 v0.0.0-20220909003341-f21342109be1
)

require (
	github.com/Henry-Sarabia/apicalypse v1.0.2 // indirect
	github.com/Henry-Sarabia/blank v3.0.0+incompatible // indirect
	github.com/Henry-Sarabia/sliceconv v1.0.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20220930213112-107f3e3c3b0b // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
