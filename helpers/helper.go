package helpers

import (
	"context"
	// "fmt"
	"log"
	"os"

	// "github.com/Henry-Sarabia/igdb/v2"
	"gitlab.com/ChTBoner/libgorom/config"
	"golang.org/x/oauth2/clientcredentials"
	"golang.org/x/oauth2/twitch"
)



var (
	clientID = ""
	// Consider storing the secret in an environment variable or a dedicated storage system.
	clientSecret = ""
	oauth2Config *clientcredentials.Config
)

func twitch_token() string {
	oauth2Config = &clientcredentials.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		TokenURL:     twitch.Endpoint.TokenURL,
	}

	token, err := oauth2Config.Token(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	return token.AccessToken
}

// func listPlatforms(c igdb.Client) {
// 	count, _ := c.Platforms.Count()
	
// 	getPlatforms := igdb.ComposeOptions(
// 		igdb.SetFields("name"),
// 		igdb.SetLimit(count),
// 	)
// 	// fmt.Println(count)
// 	platforms, _ := c.Platforms.Index(getPlatforms)
	
// 	for _, platform := range platforms {
// 		fmt.Printf("%d - %s - %s\n", platform.ID, platform.Name, platform.AlternativeName)
// 	}

// }

func dir_check(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Printf("Path %s does not exist\n", path)
	} else {
		log.Printf("%s exists", path)
	}
}

func ListRoms() {
	config := config.LoadConfig()
	rom_path := config.Roms_path

	// check if rom_path is defined in env vars
	value, exists := os.LookupEnv("ROMS_PATH")
	if exists {
		rom_path = value
	}

	dir_check(rom_path)

}
// func getGames (c igdb.Client) {
// 	getName := igdb.ComposeOptions(igdb.SetFields("name", "platforms"))
// 	games, err := c.Games.Search("ocarina zelda time legend", getName)

// 	if err != nil {
// 		// handle error
// 		fmt.Println("failed")
// 	}

// 	for _, game := range games {
// 		fmt.Println(game.Platforms, game.Name)
// 	}
// }

// func main() {
// 	token := twitch_token()

// 	client:= igdb.NewClient(clientID, token, nil)
// 	listPlatforms(*client)
// 	// getGames(*client)
// }
